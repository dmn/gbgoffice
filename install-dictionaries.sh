#!/bin/bash
# This script will download and install the dictionaries
# required for gbgoffice to operate. 
# Author: Peio Popov <peio@peio.org>

# Get the dictionaries from:
DICT_URL=http://puzzle.dl.sourceforge.net/sourceforge/bgoffice/full-pack.tar.bz2

# The prefix which was used to install gbgoffice
if [ -z $1 ]; then 
	PREFIX=/usr/local
else
	PREFIX=$1 
fi	

# Working directory 
WORK_DIR=temp

# Create a working directory and go into it
mkdir $WORK_DIR
cd $WORK_DIR

# Get the archive with the dictionaries
wget $DICT_URL

if [ $? -ne 0 ]; then
	echo "Please make sure you have wget installed!";
	exit;
fi	

# Check if the download was successfull
if ! [ -f full-pack.tar.bz2 ]; then
	echo "Please make sure you are connected to internet!"
	exit;
fi

# Check if we are root
if [ `id -u` -ne 0 ]; then
	echo "You must be root to install the dictionaries!";
	exit;
fi	

# Check if the dictionary directory exists
if ! [ -d $PREFIX/share/bgoffice ]; then
	mkdir -p $PREFIX/share/bgoffice/
fi 

# Unpack and install
tar jxvf full-pack.tar.bz2
if [ $? -ne 0 ]; then 
	echo "Please make sure you have tar and bzip2 installed!"
	exit;
fi
mv full-pack/data/*  $PREFIX/share/bgoffice/

# Clean and exit
cd ..
rm -rf $WORK_DIR
echo "The dictionaries were installed successfully!"
