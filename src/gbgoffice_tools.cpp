/*************************************************************************
*
* Gbgoffice
* Copyright (C) 2004 Miroslav Yordanov <mironcho@linux-bg.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*************************************************************************/

#include "gbgoffice_tools.h"
#include "defaults.h"
#include <iostream>
#include <cstdio>

#define TMP_STR_SIZE	20


GbgofficeTools::GbgofficeTools()
{
	try {
		smile_ok = Gdk::Pixbuf::create_from_file(std::string(FILES_DIR) + "gbgoffice-smile.png");
		smile_neok = Gdk::Pixbuf::create_from_file(std::string(FILES_DIR) + "gbgoffice-neutral.png");
		smile_nok = Gdk::Pixbuf::create_from_file(std::string(FILES_DIR) + "gbgoffice-sad.png");
	}
	catch (Glib::FileError er) {
		switch (er.code()) {
			case Glib::FileError::NO_SUCH_ENTITY:
				std::cout << "Some important files are missing!" << std::endl;
				std::cout << "Did you type 'make install' before running gbgoffice?" << std::endl;
				break;
			default:
				std::cout << "Some important files are missing or unaccessible!" << std::endl;
		}
		exit(1);
	}

}

GbgofficeTools::~GbgofficeTools()
{
}


const char *GbgofficeTools::to_cp1251(Glib::ustring str)
{
	return (Glib::convert(str, "WINDOWS-1251", "UTF-8")).c_str();
}

const Glib::ustring GbgofficeTools::to_utf8(char *str)
{
	return Glib::convert(str, "UTF-8", "WINDOWS-1251");
}

const Glib::ustring GbgofficeTools::to_utf8(std::string str)
{
	return Glib::convert(str, "UTF-8", "WINDOWS-1251");
}


Glib::ustring& operator<<(Glib::ustring& str, int i)
{
	char s[TMP_STR_SIZE];
	snprintf(s, TMP_STR_SIZE - 1, "%d", i);
	str += s;
	return str;	
}

Glib::ustring& operator<<(Glib::ustring& str, double f)
{
	char s[TMP_STR_SIZE];
	snprintf(s, TMP_STR_SIZE - 1, "%f", f);
	str += s;	
	return str;	
}

Glib::ustring& operator<<(Glib::ustring& str, const char *s)
{
	str += s;	
	return str;
}

Glib::ustring& operator<<(Glib::ustring& str, Glib::ustring s)
{
	str += s;	
	return str;
}
